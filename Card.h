//
// Created by bshort092 on 2/22/2017.
//

#ifndef BINGO_CARDS_H
#define BINGO_CARDS_H
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;



class Card {
public:
    void printCard(std::ostream& out) const;
    Card(int cardIndex, int cardSize, int numberMax);
private:
    int m_cardSize;
    int m_numberMax;
    int m_cardIndex;
    vector <vector<int> > card;
    vector<int> vecOfNum;
public:
    friend class CardTester;
    friend class DeckTester;

};


#endif //BINGO_CARDS_H
