//
// Created by bshort092 on 2/22/2017.
//

#include "Card.h"
#include <ctime>
#include <iomanip>
#include <algorithm>
#include <random>

using namespace std;


Card::Card(int cardIndex, int cardSize, int numberMax) : m_cardSize(cardSize), m_numberMax(numberMax),
                                                                  m_cardIndex(cardIndex) {
     for (int i = 1; i <= m_numberMax; i++) {
            vecOfNum.push_back(i);
        }

        random_shuffle(vecOfNum.begin(), vecOfNum.end());

        for (int i = 0; i < cardSize; i++) {
            vector<int> card1;
            for (int j = 0; j < cardSize; j++) {

                card1.push_back(vecOfNum[j]);
                vecOfNum.erase(vecOfNum.begin() + j);
                random_shuffle(vecOfNum.begin(), vecOfNum.end());
            }
            card.push_back(card1);
        }
    }

void Card::printCard(std::ostream &out) const {


    out << "Card #" << m_cardIndex << endl;

    for (int i = 0; i < m_cardSize; i++) {
        for (int j = 0; j < m_cardSize; j++) {
            out << "+----+";
        }
        out << endl;
        for (int l = 0; l < m_cardSize; l++) {
            int number = card[l][i];
            out << "|" << setw(4) << left << number << "|";
        }
        out << endl;
    }
    for (int m = 0; m < m_cardSize; m++) {
        out << "+----+";
    }
    out << endl;
}