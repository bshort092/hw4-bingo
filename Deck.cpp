//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.h"

Deck::Deck(int cardSize, int cardCount, int numberMax) :
m_cardSize(cardSize), m_cardCount(cardCount), m_numberMax(numberMax)
{
    // TODO: Implement
    for (unsigned int i = 1; i  <=m_cardCount; i++) {
        Card* newCard = new Card(i, cardSize, numberMax);
        deck.push_back(newCard);
    }
}

Deck::~Deck()
{
    // TODO: Implement
    for (int i = 0; i < m_cardCount ; i++)
    {
        delete deck[i];
    }
}

void Deck::print(std::ostream& out) const
{
    // TODO: Implement
    for (int i = 0; i <m_cardCount; i++) {
        deck[i]->printCard(out);
    }
}

void Deck::print(std::ostream& out, int cardIndex) const{

    deck[cardIndex-1]->printCard(out);
    // TODO: Implement
}
