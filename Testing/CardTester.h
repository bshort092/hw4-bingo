//
// Created by bshort092 on 3/1/2017.
//

#ifndef BINGO_CARDTESTER_H
#define BINGO_CARDTESTER_H


class CardTester {
public:
    void testConstructor();
    void testDuplicateNumbers();
    void testMaxNumber();
    void test0OrNegative();
    friend class Card;
};


#endif //BINGO_CARDTESTER_H
