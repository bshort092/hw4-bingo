//
// Created by bshort092 on 3/1/2017.
//

#ifndef BINGO_DECKTESTER_H
#define BINGO_DECKTESTER_H


class DeckTester {
public:
    void testConstructor();
    void testDeckAndCard();
};


#endif //BINGO_DECKTESTER_H
