//
// Created by bshort092 on 3/1/2017.
//

#include "DeckTester.h"
#include "../Deck.h"


void DeckTester::testConstructor() {
    cout << endl << "TestSuite: DeckTester::testConstructor" << endl;
    Deck deck(3, 5, 18);

    cout << "Test Case 1" << endl;
    if (deck.m_cardSize != 3) {
        cout << "Unexpected error: Index does not match" << endl;
    }
    cout << "Test Case 2" << endl;
    if (deck.m_cardCount != 5) {
        cout << "Unexpected error: Size does not match" << endl;
    }
    cout << "Test Case 3" << endl;
    if (deck.m_numberMax != 18) {
        cout << "Unexpected error: Max number does not match" << endl;
    }
    cout << endl;
}

void DeckTester::testDeckAndCard() {
    cout << "TestSuite: DeckTester::testDeckAndCard" << endl;

    cout << "Test Case 1" << endl;

    Deck deck(3, 5, 18); //size, count, max
    Card card(1, 3, 18); //index, size, max

    if (deck.m_cardSize != card.m_cardSize) {
        cout << "Card sizes do not match" << endl;
    }

    if (deck.m_numberMax != card.m_numberMax) {
        cout << "Card max numbers do not match" << endl;
    }

    cout << "Test Case 2" << endl;

    Deck deck2(3, 5, 19);
    Card card2(1, 4, 35);

    if (deck2.m_cardSize == card2.m_cardSize) {
        cout << "Card sizes unexpectedly match" << endl;
    }

    if (deck2.m_numberMax == card2.m_numberMax) {
        cout << "Card max numbers unexpectedly match" << endl;
    }
}