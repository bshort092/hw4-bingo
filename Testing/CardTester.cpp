//
// Created by bshort092 on 3/1/2017.
//

#include <algorithm>
#include "CardTester.h"
#include "../Card.h"



void CardTester::testConstructor(){

    cout << endl << "TestSuite: CardTester::testConstructor"<<endl;

    Card card(1, 3, 18);

    cout << "Test Case 1"<< endl;
    if(card.m_cardIndex != 1){
        cout << "Unexpected error: Index does not match" << endl;
    }
    cout << "Test Case 2"<< endl;
    if(card.m_cardSize != 3){
        cout << "Unexpected error: Size does not match" << endl;
    }
    cout << "Test Case 3"<< endl;
    if(card.m_numberMax != 18){
        cout << "Unexpected error: Max number does not match" << endl;
    }
}

void CardTester::testDuplicateNumbers() {

    vector <int> testCard;
    Card card(1, 3, 18);
    testCard = card.vecOfNum;
    cout << endl << "TestSuite: CardTester::testDuplicateNumbers"<< endl;
    cout << "Test Case 1" << endl << endl;

    sort(testCard.begin(), testCard.end());

    for (int i = 0; i < testCard.size(); i++)
    {
        if(testCard[i] == testCard[i+1])
        {
            cout << "Duplicate number found ";
        }
    }
}

void CardTester::testMaxNumber() {
    cout << "TestSuite: CardTester::testMaxNumber"<< endl;
    cout << "Test Case 1" << endl;
    vector <int> testCard;
    Card card(1,3,18);
    testCard = card.vecOfNum;

    sort(testCard.begin(), testCard.end());

    for (int i = 0; i < testCard.size(); i++)
    {
        if(testCard[i] > card.m_numberMax)
        {
            cout << "Unexpected result: Number exceeds max number";
        }
    }
}

void CardTester::test0OrNegative() {

    cout << "TestSuite: CardTester::test0OrNegative" << endl;
    cout << "Test Case 1" << endl;
    vector <int> testCard;
    Card card(0,0,0);
    testCard = card.vecOfNum;

    sort(testCard.begin(), testCard.end());

    for (int i = 0; i < testCard.size(); i++)
    {
        if(testCard[i] <= 0)
        {
            cout << "Unexpected result: Zero";
        }
    }

    cout << "Test Case 2" << endl;
    vector <int> testCard2;
    Card card2(-1,-3,-4);
    testCard2 = card2.vecOfNum;

    sort(testCard2.begin(), testCard2.end());

    for (int i = 0; i < testCard2.size(); i++)
    {
        if(testCard2[i] <= 0)
        {
            cout << "Unexpected result: Negative";
        }
    }
    cout << "Test Case 3" << endl;
    vector <int> testCard3;
    Card card3(-1,0,-4);
    testCard3 = card3.vecOfNum;

    sort(testCard3.begin(), testCard3.end());

    for (int i = 0; i < testCard3.size(); i++)
    {
        if(testCard3[i] <= 0)
        {
            cout << "Unexpected result: Negative and Zero";
        }
    }
    cout << endl;
}